package ru.t1.shevyreva.tm.controller;

import ru.t1.shevyreva.tm.api.ITaskService;
import ru.t1.shevyreva.tm.model.Project;
import ru.t1.shevyreva.tm.model.Task;
import ru.t1.shevyreva.tm.util.TerminalUtil;

import java.util.List;

public final class TaskController implements ru.t1.shevyreva.tm.api.ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("[Enter name]: ");
        final String name = TerminalUtil.nextLine();
        System.out.println("[Enter description]: ");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.create(name, description);
        if (task == null) System.out.println("[ERROR]");
        else System.out.println("[OK]");
    }

    @Override
    public void showTasks() {
        System.out.println("[TASK LIST]");
        List<Task> tasks = taskService.findAll();
        int index = 1;
        for (Task task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task.getName());
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[CLEAR TASKS]");
        taskService.clear();
        System.out.println("[OK]");
    }
}
