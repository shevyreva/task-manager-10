package ru.t1.shevyreva.tm;

import ru.t1.shevyreva.tm.component.Bootstrap;

public final class Application {

    public static void main(String[] args) {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.start(args);
    }

}
