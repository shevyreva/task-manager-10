package ru.t1.shevyreva.tm.api;

import ru.t1.shevyreva.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
