package ru.t1.shevyreva.tm.api;

public interface ITaskController {

    void createTask();

    void showTasks();

    void clearTasks();
}
