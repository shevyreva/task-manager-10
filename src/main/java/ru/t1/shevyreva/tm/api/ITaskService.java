package ru.t1.shevyreva.tm.api;

import ru.t1.shevyreva.tm.model.Project;
import ru.t1.shevyreva.tm.model.Task;

import java.util.List;

public interface ITaskService extends ITaskRepository {

    Task create(String name, String description);

}
