package ru.t1.shevyreva.tm.service;

import ru.t1.shevyreva.tm.api.ITaskRepository;
import ru.t1.shevyreva.tm.api.ITaskService;
import ru.t1.shevyreva.tm.model.Task;

import java.util.List;

public final class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Task create(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null) return null;
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        add(task);
        return task;
    }

    public void add(final Task task) {
        if (task == null) return;
        taskRepository.add(task);
    }

    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    public void clear() {
        taskRepository.clear();
    }

}
